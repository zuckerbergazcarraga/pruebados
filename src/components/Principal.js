import React from 'react';
import NoticeText from './NoticeText';
import Panel from './Panel';


export default function Principal() {

    return(
        <div className="flex">
            <NoticeText/>
            <Panel/>
        </div>
    );
}