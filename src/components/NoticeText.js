import React from 'react';

export default function NoticeText() {

    return(
        <article className="relative bg-gray-100 w-64 block shadow-xl">

            <p class="text-justify p-3">
            When you are creating a website that is going to be using dynamic content (or just creating a demo), and you don’t quite have access to what that content is yet…what do you do? You fake it. You put in dummy text, and dummy images. Microsoft Word has the feature of using “=rand(10,10)” to generate random content, but that doesn’t help me in my code editor. The focus is on the design, not the content. But running your fingers around on the keyboard for random text is kind of a waste of time and it doesn’t really yield result that shows “for placement only” type look. Besides, you need tags in there to get what you really wanted.
            </p>
            <p class="text-justify p-3">
            When you are creating a website that is going to be using dynamic content (or just creating a demo), and you don’t quite have access to what that content is yet…what do you do? You fake it. You put in dummy text, and dummy images. Microsoft Word has the feature of using “=rand(10,10)” to generate random content, but that doesn’t help me in my code editor. The focus is on the design, not the content. But running your fingers around on the keyboard for random text is kind of a waste of time and it doesn’t really yield result that shows “for placement only” type look. Besides, you need tags in there to get what you really wanted.
            </p>
            <p>
                <div class="mx-3 h-20 border">
                    anuncio
                </div>
            </p>
            <p class="text-justify p-3">
            When you are creating a website that is going to be using dynamic content (or just creating a demo), and you don’t quite have access to what that content is yet…what do you do? You fake it. You put in dummy text, and dummy images. Microsoft Word has the feature of using “=rand(10,10)” to generate random content, but that doesn’t help me in my code editor. The focus is on the design, not the content. But running your fingers around on the keyboard for random text is kind of a waste of time and it doesn’t really yield result that shows “for placement only” type look. Besides, you need tags in there to get what you really wanted.
            </p>
        </article>
    );
}