import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import './styles/salida.css';

ReactDOM.render(
  
    <App />,
  
  document.getElementById('root')
);


