import React from 'react';
import Header from '../components/Header';
import Principal from '../components/Principal';
import Footer from '../components/Footer';

export default function Notice(){ //si esto lo vas e importar en un lugar, obvio lo tienes que exportar
    
    return(
        <div>
            <Header/>
            <Principal/>
            <Footer/>
        </div>
        
    );
}